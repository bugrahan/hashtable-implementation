public class MyHashTable {

    protected int TABLE_SIZE;

    protected MyHashTable(float LF, float size) {
        this.TABLE_SIZE = (int) (size / LF);
    }

    protected int hash1(int key) {
        return key % this.TABLE_SIZE;
    }

    protected int doubleHashFunction(int key) {
        int m = 1 + (key % (this.TABLE_SIZE - 1));
        return m;
    }

}
