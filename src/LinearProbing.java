public class LinearProbing extends MyHashTable{

    private Employee[] table;

    public LinearProbing(float LF, float size){
        super(LF, size);
        this.table = new Employee[this.TABLE_SIZE];
    }

    public void put(int key, Employee e){
        int i = this.hash1(key);
        int m;
        for(m = i; this.table[m] != null; m = (m+1) % this.TABLE_SIZE);
        this.table[m] = e;
    }

    public Employee get(int key){
        System.out.println("LINEAR PROBING:");
        int i = this.hash1(key);
        int comparison = 0;
        for(int m = i; this.table[m] != null; m = (m+1) % this.TABLE_SIZE){
            comparison++;
            if(key == this.table[m].getPhone()){
                System.out.printf("Key found with %d comparisons\n", comparison);
                return this.table[m];
            }
                
        }
        System.out.println("Key not found");
        return null;
    }

    public void print(){
        System.out.println("Hashtable for Linear Probing");
        for(int i = 0; i < this.TABLE_SIZE; i++){
            System.out.printf("[%d]--->",i);
            if(this.table[i] == null)
                System.out.print("null");
            else
                System.out.print(this.table[i].getPhone());
            System.out.println();
        }

    }

}