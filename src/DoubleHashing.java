public class DoubleHashing extends MyHashTable {

    private Employee[] table;

    public DoubleHashing(float LF, float size) {
        super(LF, size);
        this.table = new Employee[this.TABLE_SIZE];
    }

    public void put(int key, Employee e) {
        int cnt = 0;
        int i;
        for (i = (this.hash1(key) + cnt * this.doubleHashFunction(key))
                % this.TABLE_SIZE; this.table[i] != null; i = (this.hash1(key) + ++cnt * this.doubleHashFunction(key))
                        % this.TABLE_SIZE)
            ;
        this.table[i] = e;

    }

    public Employee get(int key) {
        System.out.println("DOUBLE HASHING:");
        int cnt = 0;
        int comparison = 0;
        for (int i = (this.hash1(key) + cnt * this.doubleHashFunction(key))
                % this.TABLE_SIZE; this.table[i] != null; i = (this.hash1(key) + ++cnt * this.doubleHashFunction(key))
                        % this.TABLE_SIZE) {
            comparison++;
            if (this.table[i].getPhone() == key){
                System.out.printf("Key found with %d comparisons\n", comparison);
                return this.table[i];
            }
                
        }
        System.out.println("Key not found");
        return null;
    }

    public void print() {
        System.out.println("Hashtable for Double Hashing");
        for (int i = 0; i < this.TABLE_SIZE; i++) {
            System.out.printf("[%d]--->", i);
            if (this.table[i] == null)
                System.out.print("null");
            else
                System.out.print(this.table[i].getPhone());
            System.out.println();
        }
    }
}