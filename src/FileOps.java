import java.io.*;

public class FileOps {

    public static void readFile(String[] args) {
        String fileName = args[0];
        float lf1 = Float.parseFloat(args[1]);
        float lf2 = Float.parseFloat(args[2]);
        int key = Integer.parseInt(args[3]);
        try {
            String fileContent = "";
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            String line = br.readLine();
            while (line != null) {
                fileContent += line + "\n";
                line = br.readLine();
            }
            br.close();

            String[] lines = fileContent.split("\n");

            System.out.printf("%s,LF=%s,LF2=%s,%s\n", args[0], args[1], args[2], args[3]);

            createTables(lines, key, lf1, lf2);
        } catch (Exception e) {
            System.out.println("Error!");
        }
    }

    public static void createTables(String[] lines, int key, float lf1, float lf2) {
        SeparateChaining sc = new SeparateChaining(lf1, lines.length - 1); // create separate chaining table
        LinearProbing lp = new LinearProbing(lf2, lines.length - 1); // create linear probing table
        DoubleHashing dh = new DoubleHashing(lf2, lines.length - 1); // create double hashing table
        for (int i = 1; i < lines.length; i++) {
            String[] tmp = lines[i].split(" "); // split lines into fields employee code, NRIC, phone
            Employee e = new Employee(tmp[0], tmp[1], tmp[2]); // create employee object with corresponding input line
            sc.put(e.getPhone(), e); // put to separate chaining table
            lp.put(e.getPhone(), e); // put to linear probing table
            dh.put(e.getPhone(), e); // put to double hashing table
        }
        System.out.println("PART1");
        sc.print(); // print separate chaining table
        System.out.println("PART2");
        lp.print(); // print linear probing table
        dh.print(); // print double hashing table

        long startTime, endTime, duration;
        Employee toSearch;

        // Search given key in all three tables
        // and print cpu time taken
        startTime = System.nanoTime();
        toSearch = sc.get(key);
        endTime = System.nanoTime();
        duration = (endTime - startTime);
        System.out.printf("CPU time taken to search=%.1f ns\n", (float) duration);
        startTime = System.nanoTime();
        toSearch = lp.get(key);
        endTime = System.nanoTime();
        duration = (endTime - startTime);
        System.out.printf("CPU time taken to search=%.1f ns\n", (float) duration);
        startTime = System.nanoTime();
        toSearch = dh.get(key);
        endTime = System.nanoTime();
        duration = (endTime - startTime);
        System.out.printf("CPU time taken to search=%.1f ns\n", (float) duration);

    }

}