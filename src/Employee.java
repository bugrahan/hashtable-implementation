public class Employee {

    private String employeeCode;
    private String NRIC;
    private String phone;

    public Employee(String employeeCode, String NRIC, String phone) {
        this.employeeCode = employeeCode;
        this.NRIC = NRIC;
        this.phone = phone;
    }

    public String getEmployeeCode() {
        return this.employeeCode;
    }

    public String getNRIC() {
        return this.NRIC;
    }

    public int getPhone() {
        return Integer.parseInt(this.phone);
    }

}
