public class SeparateChaining extends MyHashTable{
    
    private LinkedList LL[];
    
    public SeparateChaining(float LF, float size) {
        super(LF, size);
        this.LL = new LinkedList[this.TABLE_SIZE];
        for (int i = 0; i < this.TABLE_SIZE; i++)
            this.LL[i] = new LinkedList();
    }
    
    public void put(int key, Employee e) {
        int i = this.hash1(key);
        this.LL[i].insert(e);
    }
    
    public Employee get(int key) {
        System.out.println("SEPARATE CHAINING:");
        int i = this.hash1(key);
        int comparison = 0;
        Node head = this.LL[i].getHead();
        while(head != null){
            Employee e = head.getE();
            comparison++;
            if(key == e.getPhone()){
                System.out.printf("Key found with %d comparisons\n", comparison);
                return e;
            }
                
            head = head.getNext();
        }
        System.out.println("Key not found");
        return null;
    }
    
    public void print(){
        Node tmp;
        for(int i = 0; i < this.TABLE_SIZE; i++){
            tmp = this.LL[i].getHead();
            System.out.printf("[Chain %d]: ",i);
            if(tmp == null){
                System.out.print("null");
            }
            while(tmp != null){
                System.out.printf("%d",tmp.getE().getPhone());
                tmp = tmp.getNext();
                if(tmp != null)
                    System.out.print("---->");
            }
            System.out.println();
        }
    }


}
