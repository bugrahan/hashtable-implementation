public class LinkedList {

    private Node head;

    public LinkedList() {
        this.head = null;
    }

    public void insert(Employee e) {
        Node newNode = new Node(e);

        if (this.head == null)
            this.head = newNode;
        else {
            Node temp = this.head;
            while(temp.getNext() != null)
                temp = temp.getNext();

            temp.setNext(newNode);
            //this.head = newNode;
            //this.head.setNext(temp);
        }
    }

    public Node getHead(){
        return this.head;
    }

}

class Node {

    private Employee e;
    private Node next;

    public Node(Employee e) {
        this.e = e;
        this.next = null;
    }

    public Employee getE() {
        return this.e;
    }

    public Node getNext() {
        return this.next;
    }

    public void setVal(Employee e) {
        this.e = e;
    }

    public void setNext(Node next) {
        this.next = next;
    }

}
